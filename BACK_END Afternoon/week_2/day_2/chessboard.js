function makeChessboard(chessFilter) {
  const chess = ["| Benteng", "| Kuda", "| Gajah", "| Menteri", "| Raja", "| Gajah", "| Kuda", "| Benteng"];
  const pion = ["pion"];
  const color = ["hitam ", "putih"];
  let chessboard = [];

  switch (chessFilter) {
    case "filterPejabatHitam":
      pejabatHitam = 0;
      while (pejabatHitam < chess.length) {
        chessboard += "\n" + `${chess[pejabatHitam]}_${color[0]}`;

        pejabatHitam += 1;
      }
    case "filterPionHitam":
      let pionHitam = 0;
      while (pionHitam < chess.length) {
        chessboard += "\n" + `${pion}_${color[0]}`;

        pionHitam += 1;
      }
    case "space":
      var tag = ["# "];
      let column = 1;
      var space = 1;

      while (column <= 4) {
        chessboard += "\n" + `${tag}`;
        for (var space = 1; space < 8; space += 1) {
          chessboard += tag;
        }
        column += 1;
      }
    case "filterPionPutih":
      let pionPutih = 0;
      while (pionPutih < chess.length) {
        chessboard += "\n" + `${pion}_${color[1]}`;

        pionPutih += 1;
      }
    case "filterPejabatPutih":
      let pejabatPutih = 0;
      while (pejabatPutih < chess.length) {
        chessboard += "\n" + `${chess[pejabatPutih]}_${color[1]}`;

        pejabatPutih += 1;
      }
  }
  return chessboard;
}
printBoard = (chessFilter) => {
  return makeChessboard("filterPejabatHitam");
};
console.log(printBoard());
