// NOTE : MASIH DALAM PROSES ==============================>>>>>>>>

function change(input) {
  var satuan = [
    "",
    "Satu",
    "Dua",
    "Tiga",
    "Empat",
    "Lima",
    "Enam",
    "Tujuh",
    "Delapan",
    "Sembilan",
    "Sepuluh",
    "Sebelas",
    "Dua belas",
    "Tiga belas",
    "Empat belas",
    "Lima belas",
    "Enam belas",
    "Tujuh belas",
    "Delapan belas",
    "Sembilan belas",
  ]; // total index array satuan = 20
  var puluhan = ["", "", "Dua puluh", "Tiga puluh", "Empat puluh", "Lima puluh", "Enam puluh", "Tujuh puluh", "Delapan puluh", "Sembilan puluh"];
  // total index array puluhan = 10

  var angkaToString = input.toString(); //merubah type angka yang awalnya number ke String

  if (input < 0) throw new Error("Angka tidak boleh minus"); // jika inputan (angka) terdapat minus atau < 0 maka tampilkan (teks)
  if (input === 0) return "Nol"; // jika angka sama dengan 0 maka return "Nol"

  //Filter from 1 to 20
  if (input < 20) {
    return satuan[input];
  } //=========> jika inputan (angka) < 20 panggil array satuan yang dimana nomor index sama dengan inputan (angka).

  //Filter from 20 to 99
  if (angkaToString.length === 2) {
    return puluhan[angkaToString[0]] + " " + satuan[angkaToString[1]];
  } //==========> jika inputan angka length nya berjumlah dua maka return array puluhan yang sesuai dengan nomor index puluhan dan jika inputan lebih dari 20 maka akan ditambahkan dengan kesesuaian index array satuan

  //ratusan
  if (angkaToString.length == 3) {
    if (angkaToString[1] === "0" && angkaToString[2] === "0") return satuan[angkaToString[0]] + " ratus";
    else return satuan[angkaToString[0]] + " ratus " + change(+(angkaToString[1] + angkaToString[2]));
  } // role nya sama dengan yang line 42 hanya saja ada penambahan length dan teks manual "ratus"

  //ribuan
  if (angkaToString.length === 4) {
    var end = +(angkaToString[1] + angkaToString[2] + angkaToString[3]);
    console.log(end);
    if (end === 0) return satuan[angkaToString[0]] + " ribu";

    return satuan[angkaToString[0]] + " ribu " + change(end);
  }
}

console.log(change(78)); //output=====> Tujuh puluh Delapan
