//Mengurutkan kata sesuai dengan urutan abjad.

function filterWord(input) {
  if (typeof input === "number") {
    console.log("Kata tidak Valid !! Number Terdeteksi");
  } else {
    input = input.split("").sort().join(""); //split=memilah atau memisahkan dr setiap huruf untuk dijadikan array, sort=mengurutkan isi array karena array memiliki nilai atau ukuran masing2, join=menggabungkan seluruh array menjadi string
    const complete = input;

    return complete;
  }
}

console.log(filterWord("budi")); //output =======> bdiu
console.log(filterWord("hartono")); //output =======> ahnoort
console.log(filterWord("developer")); //output ==========>deeeloprv
