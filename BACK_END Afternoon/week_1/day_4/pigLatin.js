const ubahKata = function (inputKata) {
  let strToArr = inputKata.split("");
  let vocal = ["a", "e", "i", "o", "u"];
  let kataBaru = "";
  for (let cekVocal = 0; cekVocal < vocal.length - 1; cekVocal++) {
    for (let cekInputKata = 0; cekInputKata < inputKata.length - 1; cekInputKata++) {
      if (inputKata[cekInputKata] === vocal[cekVocal]) {
        for (let hasilKata = cekInputKata; hasilKata < inputKata.length; hasilKata++) {
          kataBaru += inputKata[hasilKata];
        }
        for (let updateKata = 0; updateKata < cekInputKata; updateKata++) {
          kataBaru += inputKata[updateKata];
        }
        return kataBaru + "ay";
      }
    }
  }
};

console.log(ubahKata("food")); //anganpengembay
console.log(ubahKata("snap")); //apsnay

// Driver code
// console.log(pigLatin('food')) // ---> oodfay
// console.log(pigLatin('snap')) // ---> apsnay
// console.log(pigLatin('guide')) // ---> uidegay
// console.log(pigLatin('beli makanan')) // ---> elibay akananmay
// console.log(pigLatin('apel')) // ---> apel
