/**
 * Read file with Synchronous or Asynchronous, you can choose one method
 * Create new data to file dummyData.js
 * Don't replace existing data, only added the new data
 * new data ['mango','avocado','durian','guava']
 */

// write your code here

const fs = require("fs");

const updateData = '"mango", "avocado", "durian", "guava"';
const read = fs.readFileSync("dummyData.js", "utf8");
var update = read.replace("]", "");
const writeUpdate = () => {
  fs.writeFile("./dummyData.js", update + `, ${updateData}]`, (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log("Data Updated!!!");
    }
  });
};
writeUpdate();
