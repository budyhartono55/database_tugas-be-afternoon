//Mencari Bilangan Genap atau ganjil sampai (n)

/// for (conditionExpression){
//   if(condition){
//     // write your code here
//   }
// }
const checkNumber = (input) => {
  if (typeof input === "number") {
    for (let i = 1; i <= input; i += 1) {
      if (i % 2 === 0) {
        console.log(`${i} adalah bilangan genap`);
      } else {
        console.log(`${i} adalah bilangan ganjil`);
      }
    }
  } else {
    console.log("Bilangan tidak valid");
  }
};
checkNumber(20); //=========> akan memeriksa sampai jumlah inputan yang ditentukan user
