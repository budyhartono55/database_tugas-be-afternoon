//Kelipatan3

//  //Kelipatan 3 dengan pertambahan index 2
//  for (conditionExpression){
//   if(condition){
//     // write your code here
//   }
//  }
const kelipatan3 = (input) => {
  if (typeof input === "number") {
    for (let general = 1; general <= input; general += 1) {
      if (general % 3 === 0) {
        console.log(`${general} adalah bilangan kelipatan 3`);
      } else {
        console.log(general);
      }
    }
  } else {
    console.log("Bilangan tidak valid");
  }
};
kelipatan3(50); //=========> akan memeriksa sampai jumlah inputan yang ditentukan user
